# Agda KGs

1. Namesti docker. 
2. V ukazni vrstici nato `docker pull registry.gitlab.com/petkomat/agda-kgs/db_test:latest`
3. Kloniraj repozitorij (ali pa samo skopiraj `docker-compose.yml` nekam) in se v ukazni vrstici postavi v mapo, 
kjer je `docker-compose.yml`.
4. Zagon: `> docker-compose up -d`: `up`, da se zažene in `-d` za detached
    - brez `-d` vidimo dogajanje, ampak moramo potem pritisniti za končanje igre `ctrl-c`
5. Klik na `http://localhost:7474/` (malo je treba počakati, da se zažene) 
6. Prijava z uporabniškim imenom `neo4j` in geslom `test`.
7. Ko končamo z igro, pa še `> docker-compose down`


# Agda KGs

1. Install Docker Desktop. **Start** Docker Desktop.
    - If you are on Mac, it seems that following the [official instructions](https://docs.docker.com/desktop/install/mac-install/) is best :)
2. In the command line, run `docker pull registry.gitlab.com/petkomat/agda-kgs/db_test:latest`.
3. Clone the repository (or just copy the `docker-compose.yml` somewhere) and navigate to the directory in the command line where the `docker-compose.yml` is located.
4. To start the program, run `> docker-compose up -d`: `up` to start and `-d` for detached mode.
    - Without `-d`, we can see the progress (logs), but we need to press `ctrl-c` to exit.
5. Go to `http://localhost:7474/` (it may take a moment to start).
6. Log in with the username `neo4j` and password `test`.
7. When finished, run `> docker-compose down`.
